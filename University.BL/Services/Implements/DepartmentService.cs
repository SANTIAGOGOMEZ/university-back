﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using University.BL.Models;
using University.BL.Repositories;

namespace University.BL.Services.Implements
{
    public class DepartmentService:GenericService<Department>, IDepartmentService
    {
        private readonly IDepartmentsRepository departmentRepository;
        public DepartmentService(IDepartmentsRepository departmentRepository):base(departmentRepository)
        {
            this.departmentRepository = departmentRepository;
        }

        public async Task<bool> DeleteCheckOnEntity(int id)
        {
            return await departmentRepository.DeleteCheckOnEntity(id);
        }
    }
}
