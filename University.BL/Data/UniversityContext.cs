﻿using System.Data.Entity;
using University.BL.Models;

namespace University.BL.Data
{

    public class UniversityContext : DbContext
    {
        private static UniversityContext universityContext= null;
        public UniversityContext()
            : base("UniversityContext")
        {
               
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<CourseInstructor> CourseInstructors { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<OfficeAssignment> OfficeAssignments { get; set; }
        public static UniversityContext Create()
        {
            if (universityContext == null)
                universityContext = new UniversityContext();

            return universityContext;
        }

    }
}
//ATAJOS
//Ctrl + k + d FORMATEAR
//Ctrl + k + c COMENTAMOS BLOQUE
//Ctrl + k + u DESCOMENTAMOS BLOQUE
//Ctrl + k + s RODEAMOS EL CODIGO
//Ctrl + d DUPLICAR LINEAS DE CODIGO
//prop (DOBLE TAB)
//ctor (DOBLE TAB) METODO CONSTRUTOR