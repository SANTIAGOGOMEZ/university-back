﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using University.BL.Data;
using University.BL.DTOs;
using University.BL.Models;
using University.BL.Repositories.Implements;
using University.BL.Services.Implements;

namespace University.Web.Controllers
{
    public class OfficeAssignmentController : ApiController
    {
        private IMapper mapper;
        private readonly OfficeAssignmentService officeAssignmentService = new OfficeAssignmentService(new OfficeAssignmentRepository(UniversityContext.Create()));

        public OfficeAssignmentController()
        {
            this.mapper = WebApiApplication.MapperConfiguration.CreateMapper();
        }


        /// <summary>
        /// Obtiene los objetos Office 
        /// </summary>
        /// <returns>Listado de los objetos de Office</returns>
        /// <response code="200">Ok. Devuelve el listado de objetos solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(IEnumerable<OfficeAssignmentDTO>))]
        //[Route("GetAll")]
        public async Task<IHttpActionResult> GetAll()
        {
            var officeAssignments = await officeAssignmentService.GetAll();
            var officeAssignmentsDTO = officeAssignments.Select(x => mapper.Map<OfficeAssignment>(x));

            return Ok(officeAssignmentsDTO);
        }


        /// <summary>
        /// Obtiene un objeto por su Id
        /// </summary>
        /// <remarks>
        /// Aqui una descripcion mas larga si fuera necesario. Obtiene un objeto por su Id
        /// </remarks>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto Office</returns>
        /// <response code="200">Ok. Devuelve el objetos solicitado.</response>
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(StudentDTO))]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var officeAssignment = await officeAssignmentService.GetById(id);

            if (officeAssignment == null)
                return NotFound();

            var officeAssignmentDTO = mapper.Map<OfficeAssignmentDTO>(officeAssignment);

            return Ok(officeAssignmentDTO);
        }

        /// <summary>
        /// Insertar un objeto a Office 
        /// </summary>
        /// <remarks>
        /// Los campos son necesarios llenar
        /// </remarks>
        /// <returns>Objeto Insertado en Office </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se a Insertado el objeto solicitado </response>

        [HttpPost]
        public async Task<IHttpActionResult> Post(OfficeAssignmentDTO officeAssignmentDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            try
            {
                var officeAssignment = mapper.Map<OfficeAssignment>(officeAssignmentDTO);
                officeAssignment = await officeAssignmentService.Insert(officeAssignment);
                return Ok(officeAssignmentDTO);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }


        /// <summary>
        /// Actualiza los objetos de Office
        /// </summary>
        /// <remarks>
        /// Actualizar los campos seleccionados
        /// </remarks>
        /// <returns>Objeto Actualizado en Office </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se ah Insertado los objetos solicitados</response>
        /// <response code="404">Not Found. No se ah Actualizado los objetos solicitados</response>
        [HttpPut]
        public async Task<IHttpActionResult> Put(OfficeAssignmentDTO officeAssignmentDTO, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (officeAssignmentDTO.InstructorID != id)
                return BadRequest();

            var officeAssignment = await officeAssignmentService.GetById(id);

            if (officeAssignment == null)
                return NotFound();

            try
            {
                officeAssignment = mapper.Map<OfficeAssignment>(officeAssignmentDTO);
                officeAssignment = await officeAssignmentService.Update(officeAssignment);
                return Ok(officeAssignment);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }

        /// <summary>
        /// Elimina un  objeto en Office
        /// </summary>
        /// <remarks>
        /// Elimina el objeto solicitado por ID
        /// </remarks>
        /// <returns>Objeto Eliminado en Office </returns>
        /// <response code="200">OK. Elimina El objetos solicitados </response>
        /// <response code="404">Not Found. No se ah Eliminado el objeto solicitado</response>

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var flag = await officeAssignmentService.GetById(id);
            if (flag == null)
                return NotFound();

            try
            {
                if (!await officeAssignmentService.DeleteCheckOnEntity(id))
                    await officeAssignmentService.Delete(id);
                else
                    throw new Exception("Existen estudiantes o instructores relacionados a este curso");

                return Ok();
            }
            catch (Exception ex) { return InternalServerError(ex); }



        }


    }
}
