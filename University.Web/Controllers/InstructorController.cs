﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using University.BL.Data;
using University.BL.DTOs;
using University.BL.Models;
using University.BL.Repositories.Implements;
using University.BL.Services.Implements;
namespace University.Web.Controllers
{
    public class InstructorController : ApiController
    {
        private IMapper mapper;
        private readonly InstructorService instructorService = new InstructorService(new InstructorRepository(UniversityContext.Create()));

        public InstructorController()
        {
            this.mapper = WebApiApplication.MapperConfiguration.CreateMapper();
        }
        /// <summary>
        /// Obtiene los objetos Instructores 
        /// </summary>
        /// <returns>Listado de los objetos del Instructor</returns>
        /// <response code="200">Ok. Devuelve el listado de objetos solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(IEnumerable<InstructorDTO>))]
        //[Route("GetAll")]
        public async Task<IHttpActionResult> GetAll()
        {
            var Instructors = await instructorService.GetAll();
            var InstructorsDTO = Instructors.Select(x => mapper.Map<InstructorDTO>(x));

            return Ok(InstructorsDTO);
        }

        /// <summary>
        /// Obtiene un objeto por su Id
        /// </summary>
        /// <remarks>
        /// Aqui una descripcion mas larga si fuera necesario. Obtiene un objeto por su Id
        /// </remarks>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto Instructor</returns>
        /// <response code="200">Ok. Devuelve el objetos solicitado.</response>
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(InstructorDTO))]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var instructor = await instructorService.GetById(id);

            if (instructor == null)
                return NotFound();

            var instructorDTO = mapper.Map<InstructorDTO>(instructor);

            return Ok(instructorDTO);
        }

        /// <summary>
        /// Insertar un objeto a Instructor 
        /// </summary>
        /// <remarks>
        /// Los campos son necesarios llenar
        /// </remarks>
        /// <returns>Objeto Insertado en Instructor </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se a Insertado el objeto solicitado </response>

        [HttpPost]
        public async Task<IHttpActionResult> Post(InstructorDTO instructorDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            try
            {
                var instructor = mapper.Map<Instructor>(instructorDTO);
                instructor = await instructorService.Insert(instructor);
                return Ok(instructor);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }

        /// <summary>
        /// Actualiza los objetos de Instructores
        /// </summary>
        /// <remarks>
        /// Actualizar los campos seleccionados
        /// </remarks>
        /// <returns>Objeto Actualizado en Instructores </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se ah Insertado los objetos solicitados</response>
        /// <response code="404">Not Found. No se ah Actualizado los objetos solicitados</response>

        [HttpPut]
        public async Task<IHttpActionResult> Put(InstructorDTO instructorDTO, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (instructorDTO.ID != id)
                return BadRequest();

            var instructor = await instructorService.GetById(id);

            if (instructor == null)
                return NotFound();

            try
            {
                instructor = mapper.Map<Instructor>(instructorDTO);
                instructor = await instructorService.Update(instructor);
                return Ok(instructor);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }

        /// <summary>
        /// Elimina un  objeto en Instructores
        /// </summary>
        /// <remarks>
        /// Elimina el objeto solicitado por ID
        /// </remarks>
        /// <returns>Objeto Eliminado en Instructor </returns>
        /// <response code="200">OK. Elimina El objetos solicitados </response>
        /// <response code="404">Not Found. No se ah Eliminado el objeto solicitado</response>

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var flag = await instructorService.GetById(id);
            if (flag == null)
                return NotFound();

            try
            {
                if (!await instructorService.DeleteCheckOnEntity(id))
                    await instructorService.Delete(id);
                else
                    throw new Exception("Existen estudiantes o instructores relacionados a este curso");

                return Ok();
            }
            catch (Exception ex) { return InternalServerError(ex); }



        }

    }
}
