﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using University.BL.Data;
using University.BL.DTOs;
using University.BL.Models;
using University.BL.Repositories.Implements;
using University.BL.Services.Implements;

namespace University.Web.Controllers
{
    //[Authorize]
    [RoutePrefix("api/Courses")]
    public class CoursesController : ApiController
    {
        private IMapper mapper;
        private readonly CourseService courseService = new CourseService(new CourseRepository(UniversityContext.Create()));

        public CoursesController()
        {
            this.mapper = WebApiApplication.MapperConfiguration.CreateMapper();
        }

        /// <summary>
        /// Obtiene los objetos cursos 
        /// </summary>
        /// <returns>Listado de los objetos de cursos</returns>
        /// <response code="200">Ok. Devuelve el listado de objetos solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(IEnumerable<CourseDTO>))]
        //[Route("GetAll")]
        
        public async Task<IHttpActionResult> GetAll()
        {
            var courses = await courseService.GetAll();
            var coursesDTO = courses.Select(x => mapper.Map<CourseDTO>(x));

            return Ok(coursesDTO);
        }

        //[HttpGet]
        //[Route("GetAllTemp")]
        //public async Task<IHttpActionResult> GetAllTemp()
        //{
        //    var courses = await courseService.GetAll();
        //    var coursesDTO = courses.Select(x => mapper.Map<CourseDTO>(x));

        //    return Ok(coursesDTO);
        //}


        /// <summary>
        /// Obtiene un objeto por su Id
        /// </summary>
        /// <remarks>
        /// Aqui una descripcion mas larga si fuera necesario. Obtiene un objeto por su Id
        /// </remarks>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto Course</returns>
        /// <response code="200">Ok. Devuelve el objetos solicitado.</response>
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(CourseDTO))]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var course = await courseService.GetById(id);

            if (course == null)
                return NotFound();

            var courseDTO = mapper.Map<CourseDTO>(course);

            return Ok(courseDTO);
        }

        /// <summary>
        /// Insertar un objeto a Course 
        /// </summary>
        /// <remarks>
        /// Los campos son necesarios llenar
        /// </remarks>
        /// <returns>Objeto Insertado en cursos </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se a Insertado el objeto solicitado </response>

        [HttpPost]
        
        public async Task<IHttpActionResult> Post(CourseDTO courseDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            try
            {
                var course = mapper.Map<Course>(courseDTO);
                course = await courseService.Insert(course);
                return Ok(course);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }

        /// <summary>
        /// Actualiza los objetos de Courses
        /// </summary>
        /// <remarks>
        /// Actualizar los campos seleccionados
        /// </remarks>
        /// <returns>Objeto Actualizado en Courses </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se ah Insertado los objetos solicitados</response>
        /// <response code="404">Not Found. No se ah Actualizado los objetos solicitados</response>

        [HttpPut]
        public async Task<IHttpActionResult> Put(CourseDTO courseDTO, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (courseDTO.CourseID != id)
                return BadRequest();

            var course = await courseService.GetById(id);

            if (course == null)
                return NotFound();

            try
            {
                course = mapper.Map<Course>(courseDTO);
                course = await courseService.Update(course);
                return Ok(course);
            }
            catch (Exception ex) {return InternalServerError(ex);}
        }


        /// <summary>
        /// Elimina un  objeto en Courses
        /// </summary>
        /// <remarks>
        /// Elimina el objeto solicitado por ID
        /// </remarks>
        /// <returns>Objeto Eliminado en Courses </returns>
        /// <response code="200">OK. Elimina El objetos solicitados </response>
        /// <response code="404">Not Found. No se ah Eliminado el objeto solicitado</response>

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var flag = await courseService.GetById(id);
            if (flag == null)
                return NotFound();

            try
            {
                if (!await courseService.DeleteCheckOnEntity(id))
                    await courseService.Delete(id);
                else
                    throw new Exception("Existen estudiantes o instructores relacionados a este curso");

                return Ok();
            }
            catch (Exception ex) { return InternalServerError(ex); }
            

            
        }
    }
}
