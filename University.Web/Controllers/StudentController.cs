﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using University.BL.Data;
using University.BL.DTOs;
using University.BL.Models;
using University.BL.Repositories.Implements;
using University.BL.Services.Implements;

namespace University.Web.Controllers
{
    public class StudentController : ApiController
    {
        private IMapper mapper;
        private readonly StudentService studentService = new StudentService(new StudentRepository(UniversityContext.Create()));

        public StudentController()
        {
            this.mapper = WebApiApplication.MapperConfiguration.CreateMapper();
        }



        /// <summary>
        /// Obtiene los objetos cursos 
        /// </summary>
        /// <returns>Listado de los objetos de Student</returns>
        /// <response code="200">Ok. Devuelve el listado de objetos solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(IEnumerable<StudentDTO>))]
        //[Route("GetAll")]
        public async Task<IHttpActionResult> GetAll()
        {
            var students = await studentService.GetAll();
            var studentDTO = students.Select(x => mapper.Map<StudentDTO>(x));

            return Ok(studentDTO);
        }

        /// <summary>
        /// Obtiene un objeto por su Id
        /// </summary>
        /// <remarks>
        /// Aqui una descripcion mas larga si fuera necesario. Obtiene un objeto por su Id
        /// </remarks>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto Student</returns>
        /// <response code="200">Ok. Devuelve el objetos solicitado.</response>
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(StudentDTO))]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var student = await studentService.GetById(id);

            if (student == null)
                return NotFound();

            var studentsDTO = mapper.Map<StudentDTO>(student);

            return Ok(studentsDTO);
        }

        /// <summary>
        /// Insertar un objeto a Student 
        /// </summary>
        /// <remarks>
        /// Los campos son necesarios llenar
        /// </remarks>
        /// <returns>Objeto Insertado en Students </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se a Insertado el objeto solicitado </response>

        [HttpPost]
        public async Task<IHttpActionResult> Post(StudentDTO studentDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            try
            {
                var student = mapper.Map<Student>(studentDTO);
                student = await studentService.Insert(student);
                return Ok(student);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }


        /// <summary>
        /// Actualiza los objetos de Student
        /// </summary>
        /// <remarks>
        /// Actualizar los campos seleccionados
        /// </remarks>
        /// <returns>Objeto Actualizado en Student </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se ah Insertado los objetos solicitados</response>
        /// <response code="404">Not Found. No se ah Actualizado los objetos solicitados</response>

        [HttpPut]
        public async Task<IHttpActionResult> Put(StudentDTO studentDTO, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (studentDTO.ID != id)
                return BadRequest();

            var student = await studentService.GetById(id);

            if (student == null)
                return NotFound();

            try
            {
                student = mapper.Map<Student>(studentDTO);
                student = await studentService.Update(student);
                return Ok(student);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }


        /// <summary>
        /// Elimina un  objeto en Student
        /// </summary>
        /// <remarks>
        /// Elimina el objeto solicitado por ID
        /// </remarks>
        /// <returns>Objeto Eliminado en Students </returns>
        /// <response code="200">OK. Elimina El objetos solicitados </response>
        /// <response code="404">Not Found. No se ah Eliminado el objeto solicitado</response>

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var flag = await studentService.GetById(id);
            if (flag == null)
                return NotFound();

            try
            {
                if (!await studentService.DeleteCheckOnEntity(id))
                    await studentService.Delete(id);
                else
                    throw new Exception("Existen estudiantes o instructores relacionados a este curso");

                return Ok();
            }
            catch (Exception ex) { return InternalServerError(ex); }



        }

    }
}
