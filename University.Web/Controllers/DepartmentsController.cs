﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using University.BL.Data;
using University.BL.DTOs;
using University.BL.Models;
using University.BL.Repositories.Implements;
using University.BL.Services.Implements;
using AutoMapper;
using System.Web.Http.Description;

namespace University.Web.Controllers
{
    public class DepartmentsController : ApiController
    {
        private IMapper mapper;
        private readonly DepartmentService DeparmentService = new DepartmentService(new DepartmentRepository(UniversityContext.Create()));
        public DepartmentsController()
        {
            this.mapper = WebApiApplication.MapperConfiguration.CreateMapper();
        }

        /// <summary>
        /// Obtiene los objetos Departamentos 
        /// </summary>
        /// <returns>Listado de los objetos de Departamentos</returns>
        /// <response code="200">Ok. Devuelve el listado de objetos solicitado.</response>

        [HttpGet]
        [ResponseType(typeof(IEnumerable<DepartmentDTO>))]
        //[Route("GetAll")]
        public async Task<IHttpActionResult> GetAll()
        {
            var Departments = await DeparmentService.GetAll();
            var DepartmentsDTO = Departments.Select(x => mapper.Map<DepartmentDTO>(x));

            return Ok(DepartmentsDTO);
        }

        /// <summary>
        /// Obtiene un objeto por su Id
        /// </summary>
        /// <remarks>
        /// Aqui una descripcion mas larga si fuera necesario. Obtiene un objeto por su Id
        /// </remarks>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto Departamento</returns>
        /// <response code="200">Ok. Devuelve el objetos solicitado.</response>
        /// <response code="404">NotFound. No se ha encontrado el objeto solicitado.</response>



        [HttpGet]
        [ResponseType(typeof(DepartmentDTO))]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var Departments = await DeparmentService.GetById(id);

            if (Departments == null)
                return NotFound();

            var DepartmentsDTO = mapper.Map<DepartmentDTO>(Departments);

            return Ok(DepartmentsDTO);
        }

        /// <summary>
        /// Insertar un objeto a Departamento 
        /// </summary>
        /// <remarks>
        /// Los campos son necesarios llenar
        /// </remarks>
        /// <returns>Objeto Insertado en Departamentos </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se a Insertado el objeto solicitado </response>

        [HttpPost]
        public async Task<IHttpActionResult> Post(DepartmentDTO departmentDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);



            try
            {
                var department = mapper.Map<Department>(departmentDTO);
                department = await DeparmentService.Insert(department);
                return Ok(department);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }


        /// <summary>
        /// Actualiza los objetos de Departamentos
        /// </summary>
        /// <remarks>
        /// Actualizar los campos seleccionados
        /// </remarks>
        /// <returns>Objeto Actualizado en Departamentos </returns>
        /// <response code="200">OK. Inserta los objetos pedidos </response>
        /// <response code="400">Bad Request. No se ah Insertado los objetos solicitados</response>
        /// <response code="404">Not Found. No se ah Actualizado los objetos solicitados</response>

        [HttpPut]
        public async Task<IHttpActionResult> Put(DepartmentDTO departmentDTO, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (departmentDTO.DepartmentID != id)
                return BadRequest();

            var department = await DeparmentService.GetById(id);

            if (department == null)
                return NotFound();

            try
            {
                department = mapper.Map<Department>(departmentDTO);
                department = await DeparmentService.Update(department);
                return Ok(department);
            }
            catch (Exception ex) { return InternalServerError(ex); }
        }


        /// <summary>
        /// Elimina un  objeto en Departamento
        /// </summary>
        /// <remarks>
        /// Elimina el objeto solicitado por ID
        /// </remarks>
        /// <returns>Objeto Eliminado en Departamento </returns>
        /// <response code="200">OK. Elimina El objetos solicitados </response>
        /// <response code="404">Not Found. No se ah Eliminado el objeto solicitado</response>

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var flag = await DeparmentService.GetById(id);
            if (flag == null)
                return NotFound();

            try
            {
                if (!await DeparmentService.DeleteCheckOnEntity(id))
                    await DeparmentService.Delete(id);
                else
                    throw new Exception("Existen estudiantes o instructores relacionados a este curso");

                return Ok();
            }
            catch (Exception ex) { return InternalServerError(ex); }

        }

        }
    }
