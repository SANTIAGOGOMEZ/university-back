﻿using Owin;
using University.BL.Data;


namespace University.Web
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            //Configure the db context to use a singles instance per request
            app.CreatePerOwinContext(UniversityContext.Create);
        }
    }
}
